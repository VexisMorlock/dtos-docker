FROM archlinux:latest

#there are issues with the image and the GPG keys that are used here so the first few steps here are to fix that
# you may want to change the username and password because they are pretty terrible. admin:1234

RUN gpg --refresh-keys
RUN echo "keys refreshed" &&\
    pacman-key --init
RUN echo "init" &&\
    pacman-key --populate archlinux
RUN echo "populate" &&\
	pacman-key --refresh-keys; exit 0
RUN echo "p-refresh" &&\    
    pacman -Syyu vim git sudo --noconfirm
RUN echo "installed packs" &&\
    useradd -m -G wheel -s /bin/bash admin
RUN echo "user added" &&\
    echo -e '1234\n1234\n' | sudo passwd admin
RUN echo "password changed"	&&\
    echo '%wheel ALL=(ALL:ALL) NOPASSWD: ALL' >> /etc/sudoers   #may want to remove "NOPASSWD" after install
RUN echo "wheel mod"
RUN git clone https://gitlab.com/VexisMorlock/dtos-docker /home/admin/dtos
WORKDIR /home/admin/dtos
RUN echo "git" &&\
USER admin 
RUN echo "changed user" && ./dtos &&\
    echo "Completed install" 
